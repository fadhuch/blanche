import "./footer.css";
import { Link } from "react-router-dom";

export default function Footer() {
	return (
		<div className="footerContainer">
			<div className="topFooter">
				<div className="ourStory">
					<h4>OUR STORY</h4>
					<p>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor
						veniam asperiores magni
					</p>
					<div className="social">
						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="15"
							height="15"
							viewBox="0 0 24 24"
							fill="#737373"
							stroke="currentColor"
							stroke-width="2"
							stroke-linecap="round"
							stroke-linejoin="round"
							class="feather feather-facebook"
						>
							<path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path>
						</svg>

						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="15"
							height="15"
							viewBox="0 0 24 24"
							fill="#737373"
							stroke="currentColor"
							stroke-width="2"
							stroke-linecap="round"
							stroke-linejoin="round"
							class="feather feather-twitter"
						>
							<path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path>
						</svg>

						<svg
							xmlns="http://www.w3.org/2000/svg"
							width="15"
							height="15"
							viewBox="0 0 24 24"
							fill="none"
							stroke="currentColor"
							stroke-width="2"
							stroke-linecap="round"
							stroke-linejoin="round"
							class="feather feather-instagram"
						>
							<rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
							<path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
							<line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
						</svg>
					</div>
				</div>
				<div className="more">
					<h4>MORE..</h4>
					<Link to="#">About Us</Link>
					<Link to="#">Contact Us</Link>
					<Link to="#">Track My Order</Link>
					<Link to="#">FAQ</Link>
				</div>
				<div className="shop">
					<h4>SHOP</h4>
					<Link to="#">Bed Linen</Link>
					<Link to="#">Bath Towels</Link>
					<Link to="#">Bath Robes</Link>
					<Link to="#">Home Fragrances</Link>
				</div>
				<div className="newsletter">
					<h4>NEWSLETTER</h4>
					<p>
						Subscribe to recieve updates, access to <br /> exclusive deals, and
						more.
					</p>
					<form action="submit" className="newsletterForm">
						<input type="text" placeholder="Enter your email address" />
						<button type="submit">SUBSCRIBE</button>
					</form>
				</div>
			</div>
			<div className="bottomFooter">
				<h4>©2020 BLAUCHE</h4>
				<div className="razorpay">
					<img src="./assets/rozorpay.jpg" alt="" />
				</div>
			</div>
		</div>
	);
}
