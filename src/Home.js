import React, { useState, useRef } from "react";
import "./home.css";
import Navbar from "./components/Navbar";
import { Link } from "react-router-dom";
import Footer from "./components/Footer";

export default function Home() {
	const myref = useRef();
	const [pos, setPos] = useState(null);

	window.addEventListener("scroll", (event) => {
		setPos(window.scrollY);
	});

	console.log(pos);
	return (
		<>
			<div>
				<Navbar />
			</div>
			<div className="spotlight">
				<div className="spotlightImage">
					<img src="./assets/image1.jpg" alt="" />
				</div>
				<div className="spotlightImageText">
					<h3>
						Super Soft, Stylish Bed Linen <br /> Made With Love
					</h3>
				</div>
			</div>
			<div className="productContainer">
				<p>
					Our premium products got the finest quality cotton that create an
					exquisite lightweight textile that’s both soft and crisp – guaranteed
					to give you a wonderful night’s sleep.
				</p>
				<div className="products " ref={myref}>
					<div className={`linen  ${pos > 560 && "animationClass"}`}>
						<img src="./assets/product1.jpg" alt="" />
						<button>Shop Bed Linens</button>
					</div>
					<div className={`towel  ${pos > 700 && "animationClass"}`}>
						<img src="./assets/product2.jpg" alt="" />
						<button>Shop Bath Towels</button>
					</div>
					<div className={`robes  ${pos > 880 && "animationClass"}`}>
						<img src="./assets/product3.jpg" alt="" />
						<button>Shop Bath Robes</button>
					</div>
					<div className={`fragrance  ${pos > 1060 && "animationClass"}`}>
						<img src="./assets/product4.jpg" alt="" />
						<button>Shop Home Fragrances</button>
					</div>
				</div>
			</div>
			<div className="bestSellerContainer">
				<h3>Best Sellers</h3>
				<div className="bestSeller">
					<div className="card bS1">
						<div className="">
							<img src="./assets/bs1.jpg" alt="" />
						</div>
						<p>Product Name</p>
						<p>AED XXX</p>
						<p>Available in 5 colors</p>
						<div></div>
					</div>
					<div className="card bS2">
						<div className="">
							<img src="./assets/bs2.jpg" alt="" />
						</div>
						<p>Product Name</p>
						<p>AED XXX</p>
						<p>Special Offer Get 25% OFF</p>
					</div>
					<div className="card bS3">
						<div className="">
							<img src="./assets/bs3.jpg" alt="" />
						</div>
						<p>Product Name</p>
						<p>AED XXX</p>
						<p>Available in 3 colors</p>
					</div>
				</div>
			</div>
			<div className="about">
				<h5>Your Comfort Is Our #1 Priority</h5>
				<div className="layer">
					<img src="./assets/about.jpg" alt="" />
				</div>
				<div className="text">
					<div className="aboutDescription">
						<h6>Quality Products</h6>
						<p>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt
						</p>
					</div>
					<div className="aboutDescription">
						<h6>Lifetime Warranty</h6>
						<p>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt
						</p>
					</div>
					<div className="aboutDescription">
						<h6>Stress-free Shopping</h6>
						<p>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt
						</p>
					</div>
					<div className="aboutDescription">
						<h6>Fair Prices</h6>
						<p>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt
						</p>
					</div>
				</div>
			</div>
			<div className="ratingContainer">
				<div className="star">
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="17"
						height="17"
						viewBox="0 0 24 24"
						fill="#737373"
						stroke="#737373"
						stroke-width="2"
						stroke-linecap="round"
						stroke-linejoin="round"
						class="feather feather-star"
					>
						<polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
					</svg>
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="17"
						height="17"
						viewBox="0 0 24 24"
						fill="#737373"
						stroke="#737373"
						stroke-width="2"
						stroke-linecap="round"
						stroke-linejoin="round"
						class="feather feather-star"
					>
						<polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
					</svg>{" "}
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="17"
						height="17"
						viewBox="0 0 24 24"
						fill="#737373"
						stroke="#737373"
						stroke-width="2"
						stroke-linecap="round"
						stroke-linejoin="round"
						class="feather feather-star"
					>
						<polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
					</svg>{" "}
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="17"
						height="17"
						viewBox="0 0 24 24"
						fill="#737373"
						stroke="#737373"
						stroke-width="2"
						stroke-linecap="round"
						stroke-linejoin="round"
						class="feather feather-star"
					>
						<polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
					</svg>{" "}
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="17"
						height="17"
						viewBox="0 0 24 24"
						fill="#737373"
						stroke="#737373"
						stroke-width="2"
						stroke-linecap="round"
						stroke-linejoin="round"
						class="feather feather-star"
					>
						<polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
					</svg>
				</div>
				<div className={`review  ${pos > 2700 && "animationClass"}`}>
					"This is the best towel set I've ever had; it's cool, comfortable and
					aesthetically perfect."
				</div>
				<div className="testimonials">
					<Link to="#">READ TESTIMONIALS</Link>
				</div>
				<div className="followUs">
					<h4>FOllOW US ON</h4>
					<svg
						xmlns="http://www.w3.org/2000/svg"
						width="20"
						height="20"
						viewBox="0 0 24 24"
						fill="none"
						stroke="currentColor"
						stroke-width="2"
						stroke-linecap="round"
						stroke-linejoin="round"
						class="feather feather-instagram"
					>
						<rect x="2" y="2" width="20" height="20" rx="5" ry="5"></rect>
						<path d="M16 11.37A4 4 0 1 1 12.63 8 4 4 0 0 1 16 11.37z"></path>
						<line x1="17.5" y1="6.5" x2="17.51" y2="6.5"></line>
					</svg>
				</div>
			</div>
			<div className="footerImages">
				<div>
					<img src="./assets/footer1.jpg" alt="" />
				</div>
				<div>
					<img src="./assets/footer2.jpg" alt="" />
				</div>
				<div>
					<img src="./assets/footer3.jpg" alt="" />
				</div>
				<div>
					<img src="./assets/footer4.jpg" alt="" />
				</div>
			</div>
			<Footer />
		</>
	);
}
